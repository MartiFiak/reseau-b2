from scapy.all import *

sniff(filter="icmp", prn=lambda p:(print(f"{p[ICMP].load.decode()}"), exit()) if p.haslayer(ICMP) and len(p[ICMP].load) >= 1 else None)
