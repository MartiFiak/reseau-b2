from scapy.all import *;

sniff(filter="tcp[tcpflags] & (tcp-syn|tcp-ack) == (tcp-syn|tcp-ack)", count=1, prn=lambda p: print(f"TCP SYN ACK reçu !\n- Adresse IP src : {p[IP].src}\n- Adresse IP dst : {p[IP].dst}\n- Port TCP src : {p[TCP].sport}\n- Port TCP dst : {p[TCP].dport}"))
