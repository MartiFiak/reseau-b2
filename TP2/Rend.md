☀️ Sur **`node1.lan1.tp2`**

- afficher ses cartes réseau

```
[dora@node1.lan1.tp2 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:5a:be:2f brd ff:ff:ff:ff:ff:ff
    inet 10.1.1.11/24 brd 10.1.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe5a:be2f/64 scope link
       valid_lft forever preferred_lft forever
```

- afficher sa table de routage

```
[dora@node1.lan1.tp2 ~]$ ip r s
default via 10.1.1.254 dev enp0s8 proto static metric 100
10.1.1.0/24 dev enp0s8 proto kernel scope link src 10.1.1.11 metric 100
```

- prouvez qu'il peut joindre `node2.lan2.tp2`

```
[dora@node1.lan1.tp2 ~]$ ping 10.1.2.12
PING 10.1.2.12 (10.1.2.12) 56(84) bytes of data.
64 bytes from 10.1.2.12: icmp_seq=1 ttl=63 time=2.34 ms
64 bytes from 10.1.2.12: icmp_seq=2 ttl=63 time=5.57 ms
64 bytes from 10.1.2.12: icmp_seq=3 ttl=63 time=5.59 ms
64 bytes from 10.1.2.12: icmp_seq=4 ttl=63 time=5.26 ms
64 bytes from 10.1.2.12: icmp_seq=5 ttl=63 time=2.61 ms
^C
--- 10.1.2.12 ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4008ms
rtt min/avg/max/mdev = 2.341/4.272/5.590/1.475 ms
```

- prouvez avec un `traceroute` que le paquet passe bien par `router.tp2`

```
[dora@node1.lan1.tp2 ~]$ traceroute 10.1.2.12
traceroute to 10.1.2.12 (10.1.2.12), 30 hops max, 60 byte packets
 1  _gateway (10.1.1.254)  1.010 ms  1.032 ms  0.911 ms
 2  10.1.2.12 (10.1.2.12)  2.599 ms !X  2.002 ms !X  2.632 ms !X
```

# II. Interlude accès internet

☀️ **Sur `router.tp2`**

- prouvez que vous avez un accès internet (ping d'une IP publique)

```
[dora@router ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=111 time=28.0 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=111 time=29.9 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=111 time=28.6 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=111 time=29.2 ms
^C
--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3009ms
rtt min/avg/max/mdev = 28.043/28.945/29.887/0.683 ms
```

- prouvez que vous pouvez résoudre des noms publics (ping d'un nom de domaine public)

```
[dora@router ~]$ ping www.ynov.com
PING www.ynov.com (172.67.74.226) 56(84) bytes of data.
64 bytes from 172.67.74.226 (172.67.74.226): icmp_seq=1 ttl=57 time=14.4 ms
64 bytes from 172.67.74.226 (172.67.74.226): icmp_seq=2 ttl=57 time=17.2 ms
64 bytes from 172.67.74.226 (172.67.74.226): icmp_seq=3 ttl=57 time=18.3 ms
^C
--- www.ynov.com ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2005ms
rtt min/avg/max/mdev = 14.401/16.637/18.264/1.635 ms
```

☀️ **Accès internet LAN1 et LAN2**

- ajoutez une route par défaut sur les deux machines du LAN1

```
[dora@node1.lan1.tp2 ~]$ sudo ip route add default via 10.1.1.254 dev enp0s8
[sudo] password for dora:

[dora@node1.lan1.tp2 ~]$ ip r s
default via 10.1.1.254 dev enp0s8
default via 10.1.1.254 dev enp0s8 proto static metric 100
10.1.1.0/24 dev enp0s8 proto kernel scope link src 10.1.1.11 metric 100
```

```
[dora@node2.lan1.tp2 ~]$ sudo ip route add default via 10.1.1.254 dev enp0s8

[dora@node2.lan1.tp2 ~]$ ip r s | grep default
default via 10.1.1.254 dev enp0s8
default via 10.1.1.254 dev enp0s8 proto static metric 100
```

- ajoutez une route par défaut sur les deux machines du LAN2

```
[dora@node1.lan2.tp2 ~]$ sudo ip route add default via 10.1.2.254 dev enp0s8

[dora@node1.lan2.tp2 ~]$ ip r s | grep default
default via 10.1.2.254 dev enp0s8
default via 10.1.2.254 dev enp0s8 proto static metric 100
```

```
[dora@node2.lan2.tp2 ~]$ sudo ip route add default via 10.1.2.254 dev enp0s8

[dora@node2.lan2.tp2 ~]$ ip r s | grep default
default via 10.1.2.254 dev enp0s8
default via 10.1.2.254 dev enp0s8 proto static metric 100
```

- configurez l'adresse d'un serveur DNS que vos machines peuvent utiliser pour résoudre des noms

```
[dora@node2.lan1.tp2 ~]$ tail -n 1 /etc/resolv.conf
router 10.1.1.254
```

- dans le compte-rendu, mettez-moi que la conf des points précédents sur `node2.lan1.tp2`

```
[dora@node2.lan1.tp2 ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
DEVICE=enp0s8

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.1.1.12
NETMASK=255.255.255.0

GATEWAY=10.1.1.254
```

- prouvez que `node2.lan1.tp2` a un accès internet :
  - il peut ping une IP publique

```
[dora@node2.lan1.tp2 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=19.2 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=19.1 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=113 time=18.4 ms
^C
--- 8.8.8.8 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2004ms
rtt min/avg/max/mdev = 18.428/18.936/19.235/0.361 ms
```

  - il peut ping un nom de domaine public

```
[dora@node2.lan1.tp2 ~]$ ping google.com
PING google.com (142.250.201.46) 56(84) bytes of data.
64 bytes from mrs08s20-in-f14.1e100.net (142.250.201.46): icmp_seq=1 ttl=113 time=19.1 ms
64 bytes from mrs08s20-in-f14.1e100.net (142.250.201.46): icmp_seq=2 ttl=113 time=21.5 ms
^C
--- google.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 19.125/20.306/21.488/1.181 ms
```

# III. Services réseau

☀️ **Sur `dhcp.lan1.tp2`**

- n'oubliez pas de renommer la machine (`node2.lan1.tp2` devient `dhcp.lan1.tp2`)

```
[dora@dhcp.lan1.tp2 ~]$ sudo hostname dhcp.lan1.tp2
```

- changez son adresse IP en `10.1.1.253`

```
[dora@dhcp.lan1.tp2 ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s8 | grep IP
IPADDR=10.1.1.253
```

- setup du serveur DHCP
  - commande d'installation du paquet

```
[dora@dhcp.lan1.tp2 ~]$ sudo dnf install dhcp-server
```

  - fichier de conf

```
[dora@dhcp.lan1.tp2 ~]$ sudo cat /etc/dhcp/dhcpd.conf
#
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp-server/dhcpd.conf.example
#   see dhcpd.conf(5) man page
#
default-lease-time 600;
max-lease-time 72000;

authoritative;

subnet 10.1.1.0 netmask 255.255.255.0 {
  range dynamic-bootp 10.1.1.100 10.1.1.200;
  option routers 10.1.0.254;
  option domain-name-servers 1.1.1.1;
  option domain-name "wakiti"
}
```

  - service actif

  ```
  [dora@dhcp ~]$ sudo systemctl start dhcpd
[dora@dhcp ~]$ sudo systemctl enable dhcpd
Created symlink /etc/systemd/system/multi-user.target.wants/dhcpd.service → /usr/lib/systemd/system/dhcpd.service.

[dora@dhcp ~]$ systemctl status dhcpd
● dhcpd.service - DHCPv4 Server Daemon
     Loaded: loaded (/usr/lib/systemd/system/dhcpd.service; enabled; vendor preset: disabl>
     Active: active (running) since Sun 2023-10-22 21:23:01 CEST; 1h 4min ago
       Docs: man:dhcpd(8)
             man:dhcpd.conf(5)
   Main PID: 10747 (dhcpd)
     Status: "Dispatching packets..."
      Tasks: 1 (limit: 5905)
     Memory: 4.6M
        CPU: 8ms
     CGroup: /system.slice/dhcpd.service
             └─10747 /usr/sbin/dhcpd -f -cf /etc/dhcp/dhcpd.conf -user dhcpd -group dhcpd >

Oct 22 21:23:01 dhcp.lan1.tp2 dhcpd[10747]: ** Ignoring requests on enp0s9.  If this is no>
Oct 22 21:23:01 dhcp.lan1.tp2 dhcpd[10747]:    you want, please write a subnet declaration
Oct 22 21:23:01 dhcp.lan1.tp2 dhcpd[10747]:    in your dhcpd.conf file for the network seg>
Oct 22 21:23:01 dhcp.lan1.tp2 dhcpd[10747]:    to which interface enp0s9 is attached. **
Oct 22 21:23:01 dhcp.lan1.tp2 dhcpd[10747]:
Oct 22 21:23:01 dhcp.lan1.tp2 dhcpd[10747]: Listening on LPF/enp0s8/08:00:27:c3:1f:0d/10.5>
Oct 22 21:23:01 dhcp.lan1.tp2 dhcpd[10747]: Sending on   LPF/enp0s8/08:00:27:c3:1f:0d/10.5>
Oct 22 21:23:01 dhcp.lan1.tp2 dhcpd[10747]: Sending on   Socket/fallback/fallback-net
Oct 22 21:23:01 dhcp.lan1.tp2 dhcpd[10747]: Server starting service.
Oct 22 21:23:01 dhcp.lan1.tp2 systemd[1]: Started DHCPv4 Server Daemon.
  ```

☀️ **Sur `node1.lan1.tp2`**

- demandez une IP au serveur DHCP

```
[dora@node1.lan1.tp2 ~]$ sudo dhclient enp0s8
```

- prouvez que vous avez bien récupéré une IP *via* le DHCP

```
[dora@node1.lan1.tp2 ~]$ ip a | grep -A 1 dynamic
    inet 10.1.1.100/24 brd 10..1.255 scope global secondary dynamic enp0s8
       valid_lft 43251sec preferred_lft 43251sec
```

- prouvez que vous avez bien récupéré l'IP de la passerelle

```
[dora@node1.lan1.tp2 ~]$ ip r s | grep default
default via 10.1.1.254 dev enp0s8
default via 10.1.1.254 dev enp0s8 proto static metric 100
```

- prouvez que vous pouvez `ping node1.lan2.tp2`

```
[dora@node1.lan1.tp2 ~]$ ping 10.1.2.11
PING 10.1.2.11 (10.1.2.11) 56(84) bytes of data.
64 bytes from 10.1.2.11: icmp_seq=1 ttl=63 time=2.57 ms
64 bytes from 10.1.2.11: icmp_seq=2 ttl=63 time=6.38 ms
64 bytes from 10.1.2.11: icmp_seq=3 ttl=63 time=6.44 ms
^C
--- 10.1.2.11 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2006ms
rtt min/avg/max/mdev = 2.570/5.128/6.436/1.808 ms
```

## 2. Web web web

☀️ **Sur `web.lan2.tp2`**

- n'oubliez pas de renommer la machine (`node2.lan2.tp2` devient `web.lan2.tp2`)

```
[dora@node2Lan2 ~]$ sudo hostname web.lan2.tp2
```

- setup du service Web
  - installation de NGINX

```
[dora@web.lan2.tp2 ~]$ sudo dnf install nginx
Last metadata expiration check: 0:25:36 ago on Sun 22 Oct 2023 10:43:59 PM CEST.
Dependencies resolved.
===========================================================================================
 Package                  Architecture  Version                     Repository        Size
===========================================================================================
Installing:
 nginx                    x86_64        1:1.20.1-14.el9_2.1         appstream         36 k
Installing dependencies:
 nginx-core               x86_64        1:1.20.1-14.el9_2.1         appstream        565 k
 nginx-filesystem         noarch        1:1.20.1-14.el9_2.1         appstream        8.5 k
 rocky-logos-httpd        noarch        90.14-1.el9                 appstream         24 k

Transaction Summary
===========================================================================================
Install  4 Packages

Total download size: 634 k
Installed size: 1.8 M
Is this ok [y/N]: y
Downloading Packages:
(1/4): rocky-logos-httpd-90.14-1.el9.noarch.rpm            113 kB/s |  24 kB     00:00
(2/4): nginx-1.20.1-14.el9_2.1.x86_64.rpm                  163 kB/s |  36 kB     00:00
(3/4): nginx-filesystem-1.20.1-14.el9_2.1.noarch.rpm        38 kB/s | 8.5 kB     00:00
(4/4): nginx-core-1.20.1-14.el9_2.1.x86_64.rpm             3.5 MB/s | 565 kB     00:00
-------------------------------------------------------------------------------------------
Total                                                      1.0 MB/s | 634 kB     00:00
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                   1/1
  Running scriptlet: nginx-filesystem-1:1.20.1-14.el9_2.1.noarch                       1/4
  Installing       : nginx-filesystem-1:1.20.1-14.el9_2.1.noarch                       1/4
  Installing       : nginx-core-1:1.20.1-14.el9_2.1.x86_64                             2/4
  Installing       : rocky-logos-httpd-90.14-1.el9.noarch                              3/4
  Installing       : nginx-1:1.20.1-14.el9_2.1.x86_64                                  4/4
  Running scriptlet: nginx-1:1.20.1-14.el9_2.1.x86_64                                  4/4
  Verifying        : rocky-logos-httpd-90.14-1.el9.noarch                              1/4
  Verifying        : nginx-filesystem-1:1.20.1-14.el9_2.1.noarch                       2/4
  Verifying        : nginx-1:1.20.1-14.el9_2.1.x86_64                                  3/4
  Verifying        : nginx-core-1:1.20.1-14.el9_2.1.x86_64                             4/4

Installed:
  nginx-1:1.20.1-14.el9_2.1.x86_64                nginx-core-1:1.20.1-14.el9_2.1.x86_64
  nginx-filesystem-1:1.20.1-14.el9_2.1.noarch     rocky-logos-httpd-90.14-1.el9.noarch

Complete!
```

  - gestion de la racine web `/var/www/site_nul/`

```
[dora@web ~]$ sudo cat /etc/nginx/nginx.conf | grep "root"
        root         /var/www/site_nul/;

```

  - configuration NGINX

```
[dora@web ~]$ sudo cat /etc/nginx/nginx.conf | grep -v '#'

user nginx;
worker_processes auto;
error_log /var/log/nginx/error.log;
pid /run/nginx.pid;

include /usr/share/nginx/modules/*.conf;

events {
    worker_connections 1024;
}

http {
    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile            on;
    tcp_nopush          on;
    tcp_nodelay         on;
    keepalive_timeout   65;
    types_hash_max_size 4096;

    include             /etc/nginx/mime.types;
    default_type        application/octet-stream;

    include /etc/nginx/conf.d/*.conf;

    server {
        listen       80;
        listen       [::]:80;
        server_name  site_nul.com;
        root         /var/www/site_nul/;

        include /etc/nginx/default.d/*.conf;

        error_page 404 /404.html;
        location = /404.html {
        }

        error_page 500 502 503 504 /50x.html;
        location = /50x.html {
        }
    }


}
```

  - service actif

```
[dora@web.lan2.tp2 ~]$ systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
     Loaded: loaded (/usr/lib/systemd/system/nginx.service; enabled; preset: disabled)
     Active: active (running) since Sun 2023-10-22 23:20:33 CEST; 4min 36s ago
   Main PID: 45231 (nginx)
      Tasks: 2 (limit: 5905)
     Memory: 1.9M
        CPU: 31ms
     CGroup: /system.slice/nginx.service
             ├─45231 "nginx: master process /usr/sbin/nginx"
             └─45232 "nginx: worker process"

Oct 22 23:20:33 web.lan2.tp2 systemd[1]: Starting The nginx HTTP and reverse proxy server.>
Oct 22 23:20:33 web.lan2.tp2 nginx[45229]: nginx: the configuration file /etc/nginx/nginx.>
Oct 22 23:20:33 web.lan2.tp2 nginx[45229]: nginx: configuration file /etc/nginx/nginx.conf>
Oct 22 23:20:33 web.lan2.tp2 systemd[1]: Started The nginx HTTP and reverse proxy server.
```

  - ouverture du port firewall

```
[dora@web.lan2.tp2 ~]$ sudo firewall-cmd --add-service=http --permanent
success
[dora@web.lan2.tp2 ~]$ sudo firewall-cmd --add-service=https --permanent
success
[dora@web.lan2.tp2 ~]$ sudo firewall-cmd --reload
success
```

- prouvez qu'il y a un programme NGINX qui tourne derrière le port 80 de la machine (commande `ss`)

```
[dora@web.lan2.tp2 ~]$ sudo ss -alnpt | grep 80
LISTEN 0      511          0.0.0.0:80        0.0.0.0:*    users:(("nginx",pid=45232,fd=6),("nginx",pid=45231,fd=6))
LISTEN 0      511             [::]:80           [::]:*    users:(("nginx",pid=45232,fd=7),("nginx",pid=45231,fd=7))
```

- prouvez que le firewall est bien configuré

```
[dora@web.lan2.tp2 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s8
  sources:
  services: cockpit dhcpv6-client http https ssh
  ports: 80/tcp
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

☀️ **Sur `node1.lan1.tp2`**

- éditez le fichier `hosts` pour que `site_nul.tp2` pointe vers l'IP de `web.lan2.tp2`

```
[dora@node1.lan1.tp2 ~]$ cat /etc/hosts | grep site
10.1.2.12   site_nul.tp2
```

- visitez le site nul avec une commande `curl` et en utilisant le nom `site_nul.tp2`

```
[dora@node1.lan1.tp2 ~]$ curl site_nul.tp2
<html>
<head><title>404 Not Found</title></head>
<body>
<center><h1>404 Not Found</h1></center>
<hr><center>nginx/1.20.1</center>
</body>
</html>
```