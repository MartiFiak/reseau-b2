# TP1 : Maîtrise réseau du poste

Pour ce TP, on va utiliser **uniquement votre poste** (pas de VM, rien, quedal, quetchi).

Le but du TP : se remettre dans le bain tranquillement en manipulant pas mal de concepts qu'on a vu l'an dernier.

C'est un premier TP *chill*, qui vous(ré)apprend à maîtriser votre poste en ce qui concerne le réseau. Faites le seul ou avec votre mate préféré bien sûr, mais jouez le jeu, faites vos propres recherches.

La "difficulté" va crescendo au fil du TP, mais la solution tombe très vite avec une ptite recherche Google si vos connaissances de l'an dernier deviennent floues.

- [TP1 : Maîtrise réseau du poste](#tp1--maîtrise-réseau-du-poste)
- [I. Basics](#i-basics)
- [II. Go further](#ii-go-further)
- [III. Le requin](#iii-le-requin)

# I. Basics

> Tout est à faire en ligne de commande, sauf si précision contraire.

☀️ **Carte réseau WiFi**

Déterminer...
```
PS C:\Users\ZerBr> ipconfig /all

Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Intel(R) Wi-Fi 6 AX201 160MHz
   Adresse physique . . . . . . . . . . . : C4-BD-E5-DC-E1-5A
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::c3cd:f69b:b2b6:8d36%11(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.76.176(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.240.0
   Bail obtenu. . . . . . . . . . . . . . : jeudi 12 octobre 2023 08:56:48
   Bail expirant. . . . . . . . . . . . . : vendredi 13 octobre 2023 08:56:46
   Passerelle par défaut. . . . . . . . . : 10.33.79.254
   Serveur DHCP . . . . . . . . . . . . . : 10.33.79.254
   IAID DHCPv6 . . . . . . . . . . . : 113556965
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-29-7E-13-D9-B4-45-06-96-86-C2
   Serveurs DNS. . .  . . . . . . . . . . : 8.8.8.8
                                       1.1.1.1
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé
```
- l'adresse MAC de votre carte WiFi

```
Adresse physique . . . . . . . . . . . : C4-BD-E5-DC-E1-5A
```
- l'adresse IP de votre carte WiFi

```
Adresse IPv4. . . . . . . . . . . . . .: 10.33.76.176(préféré)
```
- le masque de sous-réseau du réseau LAN auquel vous êtes connectés en WiFi

```
Masque de sous-réseau. . . . . . . . . : 255.255.240.0
```
  - en notation CIDR, par exemple `/16`

```
  255.255.240.0 ou /20
```
  - ET en notation décimale, par exemple `255.255.0.0`

```
  255.255.0.0
```

---

☀️ **Déso pas déso**

Pas besoin d'un terminal là, juste une feuille, ou votre tête, ou un tool qui calcule tout hihi. Déterminer...

- l'adresse de réseau du LAN auquel vous êtes connectés en WiFi

```
10.33.64.0
```
- l'adresse de broadcast

```
10.33.79.255
```
- le nombre d'adresses IP disponibles dans ce réseau

```
4094
```
---

☀️ **Hostname**

- déterminer le hostname de votre PC

```
PS C:\Users\ZerBr> hostname
MartyFiakoTron
```

---

☀️ **Passerelle du réseau**

Déterminer...

- l'adresse IP de la passerelle du réseau

```
PS C:\Users\ZerBr> ipconfig /all

Carte réseau sans fil Wi-Fi :
Passerelle par défaut. . . . . . . . . : 10.33.79.254
```

- l'adresse MAC de la passerelle du réseau

```
PS C:\Users\ZerBr> arp -a

Interface : 10.33.76.176 --- 0xb
  Adresse Internet      Adresse physique      Type
  10.33.79.254          7c-5a-1c-d3-d8-76     dynamique
```
---

☀️ **Serveur DHCP et DNS**

Déterminer...

- l'adresse IP du serveur DHCP qui vous a filé une IP

```
Serveur DHCP . . . . . . . . . . . . . : 10.33.79.254
```
- l'adresse IP du serveur DNS que vous utilisez quand vous allez sur internet

```
Serveurs DNS. . .  . . . . . . . . . . : 8.8.8.8
                                       1.1.1.1
```

---

☀️ **Table de routage**

Déterminer...

- dans votre table de routage, laquelle est la route par défaut

```
PS C:\Users\ZerBr> route print

IPv4 Table de routage
===========================================================================
Itinéraires actifs :
Destination réseau    Masque réseau  Adr. passerelle   Adr. interface Métrique
          0.0.0.0          0.0.0.0     10.33.79.254     10.33.76.176     30
```

---

![Not sure](./img/notsure.png)

# II. Go further

> Toujours tout en ligne de commande.

---

☀️ **Hosts ?**

- faites en sorte que pour votre PC, le nom `b2.hello.vous` corresponde à l'IP `1.1.1.1`

```
ZerBr@MartyFiakoTron MINGW64 /c
$ cat Windows/System32/drivers/etc/hosts.ics
## Copyright (c) 1993-2001 Microsoft Corp.
#
# This file has been automatically generated for use by Microsoft Internet
# Connection Sharing. It contains the mappings of IP addresses to host names
# for the home network. Please do not make changes to the HOSTS.ICS file.
# Any changes may result in a loss of connectivity between machines on the
# local network.
#

#192.168.137.1 MartyFiakoTron.mshome.net # 2028 5 5 12 10 49 8 264
#192.168.137.70 OnePlus-6T-Egnope-Niap.mshome.net # 2023 5 6 20 12 7 22 398



1.1.1.1 b2.hello.vous

```
- prouvez avec un `ping b2.hello.vous` que ça ping bien `1.1.1.1`

```
PS C:\Users\ZerBr> ping b2.hello.vous

Envoi d’une requête 'ping' sur b2.hello.vous [1.1.1.1] avec 32 octets de données :
Réponse de 1.1.1.1 : octets=32 temps=10 ms TTL=57
Réponse de 1.1.1.1 : octets=32 temps=10 ms TTL=57
Réponse de 1.1.1.1 : octets=32 temps=11 ms TTL=57

Statistiques Ping pour 1.1.1.1:
    Paquets : envoyés = 3, reçus = 3, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 10ms, Maximum = 11ms, Moyenne = 10ms
```

> Vous pouvez éditer en GUI, et juste me montrer le contenu du fichier depuis le terminal pour le compte-rendu.

---

☀️ **Go mater une vidéo youtube et déterminer, pendant qu'elle tourne...**

```
PS C:\Users\ZerBr> netstat -b -a -n -o | Select-String 23.192.237.215 -Context 0,1

>   UDP    0.0.0.0:53570          23.192.237.215:443                     25852
   [msedge.exe]
```


- l'adresse IP du serveur auquel vous êtes connectés pour regarder la vidéo

```23.192.237.215```

- le port du serveur auquel vous êtes connectés

```443```

- le port que votre PC a ouvert en local pour se connecter au port du serveur distant

```53570```

---

☀️ **Requêtes DNS**

Déterminer...

```
PS C:\Users\ZerBr> ping www.ynov.com

Envoi d’une requête 'ping' sur www.ynov.com [104.26.11.233] avec 32 octets de données :
Réponse de 104.26.11.233 : octets=32 temps=42 ms TTL=55
Réponse de 104.26.11.233 : octets=32 temps=61 ms TTL=55
Réponse de 104.26.11.233 : octets=32 temps=31 ms TTL=55
Réponse de 104.26.11.233 : octets=32 temps=42 ms TTL=55

Statistiques Ping pour 104.26.11.233:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 31ms, Maximum = 61ms, Moyenne = 44ms
```

- à quelle adresse IP correspond le nom de domaine `www.ynov.com`

```104.26.11.233```

> Ca s'appelle faire un "lookup DNS".

- à quel nom de domaine correspond l'IP `174.43.238.89`

```
PS C:\Users\ZerBr> nslookup 174.43.238.89 | Select-String nom

Nom :    89.sub-174-43-238.myvzw.com
```

> Ca s'appelle faire un "reverse lookup DNS".

---

☀️ **Hop hop hop**

Déterminer...

- par combien de machines vos paquets passent quand vous essayez de joindre `www.ynov.com`

```
PS C:\Users\ZerBr> tracert www.ynov.com

Détermination de l’itinéraire vers www.ynov.com [104.26.10.233]
avec un maximum de 30 sauts :

  1     4 ms     4 ms     3 ms  192.168.197.119
  2     *        *        *     Délai d’attente de la demande dépassé.
  3    47 ms    34 ms    39 ms  10.4.1.32
  4    80 ms    28 ms   127 ms  10.231.23.252
  5    39 ms    39 ms    37 ms  93.8.130.77.rev.sfr.net [77.130.8.93]
  6    62 ms    50 ms    65 ms  12.148.6.194.rev.sfr.net [194.6.148.12]
  7    57 ms    33 ms    36 ms  12.148.6.194.rev.sfr.net [194.6.148.12]
  8    86 ms    39 ms    51 ms  141.101.67.48
  9    67 ms    35 ms    38 ms  172.71.124.4
 10    98 ms    43 ms    49 ms  104.26.10.233

Itinéraire déterminé.
```

```
9 machines
```

---

☀️ **IP publique**

Déterminer...

- l'adresse IP publique de la passerelle du réseau (le routeur d'YNOV donc si vous êtes dans les locaux d'YNOV quand vous faites le TP)

```
PS C:\Users\ZerBr> ipconfig /all | Select-String Wi-Fi -Context 0,9

> Carte réseau sans fil Wi-Fi :

     Passerelle par défaut. . . . . . . . . : 10.33.79.254
```

---

☀️ **Scan réseau**

Déterminer...

- combien il y a de machines dans le LAN auquel vous êtes connectés

```
PS C:\Users\ZerBr> arp -a

Interface : 10.5.1.1 --- 0x8
  Adresse Internet      Adresse physique      Type
  10.5.1.255            ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 10.33.76.176 --- 0xb
  Adresse Internet      Adresse physique      Type
  10.33.71.104          6c-94-66-1f-be-8b     dynamique
  10.33.74.177          8c-85-90-cf-7d-95     dynamique
  10.33.76.225          d8-f3-bc-54-c7-8f     dynamique
  10.33.79.254          7c-5a-1c-d3-d8-76     dynamique
  10.33.79.255          ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique

Interface : 192.168.207.1 --- 0xf
  Adresse Internet      Adresse physique      Type
  192.168.207.254       00-50-56-e9-1e-6b     dynamique
  192.168.207.255       ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique

Interface : 192.168.145.1 --- 0x18
  Adresse Internet      Adresse physique      Type
  192.168.145.254       00-50-56-e6-7d-31     dynamique
  192.168.145.255       ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
```

> Allez-y mollo, on va vite flood le réseau sinon. :)

![Stop it](./img/stop.png)

# III. Le requin

Faites chauffer Wireshark. Pour chaque point, je veux que vous me livrez une capture Wireshark, format `.pcap` donc.

Faites *clean* 🧹, vous êtes des grands now :

- livrez moi des captures réseau avec uniquement ce que je demande et pas 40000 autres paquets autour
  - vous pouvez sélectionner seulement certains paquets quand vous enregistrez la capture dans Wireshark
- stockez les fichiers `.pcap` dans le dépôt git et côté rendu Markdown, vous me faites un lien vers le fichier, c'est cette syntaxe :

```markdown
[Lien vers capture ARP](./captures/arp.pcap)
```

---

☀️ **Capture ARP**

- 📁 fichier `arp.pcap`
- capturez un échange ARP entre votre PC et la passerelle du réseau

> Si vous utilisez un filtre Wireshark pour mieux voir ce trafic, précisez-le moi dans le compte-rendu.

```utilisation du filtre arp```

[ARP](./arp.pcap)

---

☀️ **Capture DNS**

- 📁 fichier `dns.pcap`
- capturez une requête DNS vers le domaine de votre choix et la réponse
- vous effectuerez la requête DNS en ligne de commande

> Si vous utilisez un filtre Wireshark pour mieux voir ce trafic, précisez-le moi dans le compte-rendu.

```utilisation du filtre dns```

[DNS](./dns.pcap)

---

☀️ **Capture TCP**

- 📁 fichier `tcp.pcap`
- effectuez une connexion qui sollicite le protocole TCP
- je veux voir dans la capture :
  - un 3-way handshake
  - un peu de trafic
  - la fin de la connexion TCP

> Si vous utilisez un filtre Wireshark pour mieux voir ce trafic, précisez-le moi dans le compte-rendu.

```utilisation du filtre tcp```

[TCP](./tcp.pcap)

---