# TP7 SECU : Accès réseau sécurisé

# I. VPN

🌞 **Monter un serveur VPN Wireguard sur `vpn.tp7.secu`**

- ajoutez une carte NAT à `vpn.tp7.secu`
- le réseau VPN doit être `10.7.2.0/24`
- [j'ai écrit un TP pour les B1 pour monter un serveur Wireguard, go vous en inspirer](https://gitlab.com/it4lik/b1-reseau-2023/-/blob/master/tp/7/vpn.md)
- vous devez pouvoir démarrer le serveur VPN avec un `systemctl start`

```
vpn.tp7.secu :

[dora@vpn ~]$ sudo modprobe wireguard

[dora@vpn ~]$ echo wireguard | sudo tee /etc/modules-load.d/wireguard.conf
wireguard

[dora@vpn ~]$ sudo cat /etc/sysctl.conf
net.ipv4.ip_forward = 1
net.ipv6.conf.all.forwarding = 1

[dora@vpn ~]$ sudo sysctl -p
net.ipv4.ip_forward = 1
net.ipv6.conf.all.forwarding = 1

[dora@vpn ~]$ wg genkey | sudo tee /etc/wireguard/server.key
+LEb6pFKV4ZuKFfKrgp4gLkn+iDrGk29HTzZBNseDmU=

[dora@vpn ~]$ sudo chmod 0400 /etc/wireguard/server.key

[dora@vpn ~]$ sudo cat /etc/wireguard/server.key | wg pubkey | sudo tee /etc/wireguard/server.pub
ZUWG5I+wC3tCLd31hvWCPx65b5Pf/avTr4cJ9zbYqm4=

[dora@vpn ~]$ sudo mkdir -p /etc/wireguard/clients

[dora@vpn ~]$ wg genkey | sudo tee /etc/wireguard/clients/martine.key
2PoEJsPPoSrcViFC957hXW5oNy+JlrbACQ6zEGuISWw=

[dora@vpn ~]$ sudo chmod 0400 /etc/wireguard/clients/martine.key

[dora@vpn ~]$ sudo cat /etc/wireguard/clients/martine.key | wg pubkey | sudo tee /etc/wireguard
/clients/martine.pub
YLdhFWitE+DtaDMUBs+QQCxAUtC8R0T4DDbZbqG/b1M=

[dora@vpn ~]$ sudo cat /etc/wireguard/wg0.conf
[Interface]
Address = 10.7.2.1/24
PrivateKey = +LEb6pFKV4ZuKFfKrgp4gLkn+iDrGk29HTzZBNseDmU=

[Peer]
PublicKey = YLdhFWitE+DtaDMUBs+QQCxAUtC8R0T4DDbZbqG/b1M=
AllowedIPs = 10.7.2.11/32

[Peer]
PublicKey = EVdLeAW3gfwyHOcxhY1p5SckTpH5VFQPhWpBWMInujE=
AllowedIPs = 10.7.2.100/32
```

🌞 **Client Wireguard sur `martine.tp7.secu`**

- configurez un client Wireguard sur `martine.tp7.secu`
- vous pouvez ajouter TEMPORAIREMENT une carte NAT le temps de faire une install (n'oubliez pas de l'enlever après)
- une fois connecté au VPN vous pouvez accéder à Internet en passant par le serveur

```
martine.tp7.secu :

[dora@martine ~]$ sudo cat /etc/wireguard/martine.conf
[Interface]
PrivateKey = 2PoEJsPPoSrcViFC957hXW5oNy+JlrbACQ6zEGuISWw=
Address = 10.7.2.11/24

[Peer]
PublicKey = ZUWG5I+wC3tCLd31hvWCPx65b5Pf/avTr4cJ9zbYqm4=
Endpoint = 10.7.1.0:51820
AllowedIPs = 0.0.0.0/0, ::/0

[dora@martine ~]$ sudo wg-quick up martine.conf
wg-quick: `/etc/wireguard/martine.conf.conf' does not exist
[dora@martine ~]$ sudo wg-quick up martine
[#] ip link add martine type wireguard
[#] wg setconf martine /dev/fd/63
[#] ip -4 address add 10.7.2.11/24 dev martine
[#] ip link set mtu 1420 up dev martine
[#] wg set martine fwmark 51820
[#] ip -6 route add ::/0 dev martine table 51820
[#] ip -6 rule add not fwmark 51820 table 51820
[#] ip -6 rule add table main suppress_prefixlength 0
[#] nft -f /dev/fd/63
[#] ip -4 route add 0.0.0.0/0 dev martine table 51820
[#] ip -4 rule add not fwmark 51820 table 51820
[#] ip -4 rule add table main suppress_prefixlength 0
[#] sysctl -q net.ipv4.conf.all.src_valid_mark=1
[#] nft -f /dev/fd/63

[dora@martine ~]$ sudo systemctl enable wg-quick@martine.conf
Created symlink /etc/systemd/system/multi-user.target.wants/wg-quick@martine.conf.service → /usr/lib/systemd/system/wg-quick@.service.

[dora@martine ~]$ wg-quick up wireguard/martine.conf
wg-quick must be run as root. Please enter the password for dora to continue:
Warning: `/home/dora/wireguard/martine.conf' is world accessible
[#] ip link add martine type wireguard
[#] wg setconf martine /dev/fd/63
[#] ip -4 address add 10.7.2.11/24 dev martine
[#] ip link set mtu 1420 up dev martine
[#] wg set martine fwmark 51820
[#] ip -6 route add ::/0 dev martine table 51820
[#] ip -6 rule add not fwmark 51820 table 51820
[#] ip -6 rule add table main suppress_prefixlength 0
[#] nft -f /dev/fd/63
[#] ip -4 route add 0.0.0.0/0 dev martine table 51820
[#] ip -4 rule add not fwmark 51820 table 51820
[#] ip -4 rule add table main suppress_prefixlength 0
[#] sysctl -q net.ipv4.conf.all.src_valid_mark=1
[#] nft -f /dev/fd/63
```

🌞 **Client Wireguard sur votre PC**

- configurez un client Wireguard sur votre PC
- vous devriez pouvoir conserver votre accès internet (sans passer par le VPN) ET ping `martine.tp7.secu` en utilisant le VPN

```
[Interface]
PrivateKey = GPLqT6evpDkdfFMuY401kygVTic4Wa47vnzuMkKMQn4=

[Peer]
PublicKey = ZUWG5I+wC3tCLd31hvWCPx65b5Pf/avTr4cJ9zbYqm4=
AllowedIPs = 0.0.0.0/0
Endpoint = 10.7.2.1:51820
```

# II. SSH

## 1. Setup

🌞 **Générez des confs Wireguard pour tout le monde**

- tout le monde doit pouvoir se ping en utilisant les IPs du VPN
- il serait ptet malin de faire un script non ? J'propose hein.

```
[dora@martine ~]$ sudo cat generate_wireguard_config.sh
generate_keys() {
    private_key=$(wg genkey)
    public_key=$(echo "$private_key" | wg pubkey)
    echo "PrivateKey = $private_key"
    echo "PublicKey = $public_key"
}

echo "[Interface]"
generate_keys

echo -e "\n[Peer]"
generate_keys

echo -e "\n[Peer]"
generate_keys

echo -e "\n[Peer]"
generate_keys

[dora@martine ~]$ chmod +x generate_wireguard_config.sh
```

## 3. Connexion par clé

🌞 **Générez une nouvelle paire de clés pour ce TP**

- vous les utiliserez pour vous connecter aux machines
- vous n'utiliserez **PAS** l'algorithme RSA
- faites des recherches pour avoir l'avis de gens qualifiés sur l'algo à choisir en 2023 pour avoir la "meilleure" clé (sécurité et perfs)

```
[dora@martine ~]$ ssh-keygen -t ed25519 -f ~/.ssh/id_ed25519_tp7

[dora@martine ~]$ ssh-copy-id martine.tp7.secu
[dora@martine ~]$ ssh-copy-id vpn.tp7.secu
[dora@martine ~]$ ssh-copy-id web.tp7.secu
```

```
[dora@martine ~]$ sudo cat /etc/ssh/sshd_config
# /etc/ssh/sshd_config

Port 22
Protocol 2
HostKey /etc/ssh/ssh_host_rsa_key
HostKey /etc/ssh/ssh_host_ecdsa_key
HostKey /etc/ssh/ssh_host_ed25519_key
PermitRootLogin yes
PasswordAuthentication yes
PermitEmptyPasswords no
ChallengeResponseAuthentication no
UsePAM yes
X11Forwarding yes
PrintMotd no
AcceptEnv LANG LC_*
Subsystem sftp /usr/libexec/openssh/sftp-server
```

## 4. Conf serveur SSH

🌞 **Changez l'adresse IP d'écoute**

- sur toutes les machines
- vos serveurs SSH ne doivent être disponibles qu'au sein du réseau VPN
- prouvez que vous ne pouvez plus accéder à une sesion SSH en utilisant l'IP host-only (obligé de passer par le VPN)

🌞 **Améliorer le niveau de sécurité du serveur**

- sur toutes les machines
- mettre en oeuvre au moins 3 configurations additionnelles pour améliorer le niveau de sécurité
- 3 lignes (au moins) à changer quoi
- le doc est vieux, mais en dehors des recommendations pour le chiffrement le reste reste très cool : [l'ANSSI avait édité des recommendations pour une conf OpenSSH](https://cyber.gouv.fr/publications/openssh-secure-use-recommendations)

# III. HTTP

## 1. Initial setup

🌞 **Monter un bête serveur HTTP sur `web.tp7.secu`**

- avec NGINX
- une page d'accueil HTML avec écrit "toto" ça ira
- **il ne doit écouter que sur l'IP du VPN**
- une conf minimale ressemble à ça :

```
[dora@web ~]$ sudo dnf install nginx
[dora@web ~]$ sudo cat /etc/nginx/nginx.co# /etc/nginx/nginx.conf

server {
    listen 10.7.1.13:80;
    server_name web.tp7.secu;

    location / {
        root /var/www/site_nul;
        index index.html;
    }
}f

[dora@web ~]$ sudo systemctl restart nginx
```

🌞 **Site web joignable qu'au sein du réseau VPN**

- le site web ne doit écouter que sur l'IP du réseau VPN
- le trafic à destination du port 80 n'est autorisé que si la requête vient du réseau VPN (firewall)
- prouvez qu'il n'est pas possible de joindre le site sur son IP host-only

🌞 **Accéder au site web**

- depuis votre PC, avec un `curl`
- vous êtes normalement obligés d'être co au VPN pour accéder au site

```
C:\Users\ZerBr> curl http://10.7.1.13
<html><body><h1>toto</h1></body></html>
```

## 2. Génération de certificat et HTTPS

### A. Préparation de la CA

🌞 **Générer une clé et un certificat de CA**

```
[dora@web ~]$ openssl genrsa -des3 -out CA.key 4096
[dora@web ~]$ openssl req -x509 -new -nodes -key CA.key -sha256 -days 1024 -out CA.pem
```

### B. Génération du certificat pour le serveur web

🌞 **Générer une clé et une demande de signature de certificat pour notre serveur web**

```
[dora@web ~]$ openssl req -new -nodes -out web.tp7.secu.csr -newkey rsa:4096 -keyout web.tp7.secu.key

[dora@web ~]$ echo -e "authorityKeyIdentifier=keyid,issuer\nbasicConstraints=CA:FALSE\nkeyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment\nsubjectAltName = @alt_names\n\n[alt_names]\nDNS.1 = web.tp7.secu\nDNS.2 = www.tp7.secu" | sudo tee v3.ext
```

🌞 **Faire signer notre certificat par la clé de la CA**

- préparez un fichier `v3.ext` qui contient :

```ext
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names

[alt_names]
DNS.1 = web.tp7.secu
DNS.2 = www.tp7.secu
```

- effectuer la demande de signature pour récup un certificat signé par votre CA :

```bash
$ openssl x509 -req -in web.tp7.secu.csr -CA CA.pem -CAkey CA.key -CAcreateserial -out web.tp7.secu.crt -days 500 -sha256 -extfile v3.ext
$ ls
# web.tp7.secu.crt c'est le certificat qu'utilisera le serveur web
```

```
[dora@web ~]$ openssl x509 -req -in web.tp7.secu.csr -CA CA.pem -CAkey CA.key -CAcreateserial -out web.tp7.secu.crt -days 500 -sha256 -extfile v3.ext
Signature ok
```

### C. Bonnes pratiques RedHat

Sur RedHat, il existe un emplacement réservé aux clés et certificats :

- `/etc/pki/tls/certs/` pour les certificats
  - pas choquant de voir du droit de lecture se balader
- `/etc/pki/tls/private/` pour les clés
  - ici, seul le propriétaire du fichier a le droit de lecture

🌞 **Déplacer les clés et les certificats dans l'emplacement réservé**

- gérez correctement les permissions de ces fichiers

```
[dora@web ~]$ sudo mv CA.pem /etc/pki/tls/certs/

[dora@web ~]$ sudo mv web.tp7.secu.crt /etc/pki/tls/certs/

[dora@web ~]$ sudo mv web.tp7.secu.key /etc/pki/tls/private/

[dora@web ~]$ sudo chmod 644 /etc/pki/tls/certs/*
[dora@web ~]$ sudo chmod 600 /etc/pki/tls/private/*

[dora@web ~]$ sudo chown root:root /etc/pki/tls/certs/*

[dora@web ~]$ sudo chown root:root /etc/pki/tls/private/*
```

### D. Config serveur Web

🌞 **Ajustez la configuration NGINX**

- le site web doit être disponible en HTTPS en utilisant votre clé et votre certificat
- une conf minimale ressemble à ça :

```nginx
server {
    server_name web.tp7.secu;

    listen 10.7.1.103:443 ssl;

    ssl_certificate /etc/pki/tls/certs/web.tp7.secu.crt;
    ssl_certificate_key /etc/pki/tls/private/web.tp7.secu.key;
    
    root /var/www/site_nul;
}
```

```
[dora@web ~]$ sudo cat /etc/nginx/nginx.conf
# /etc/nginx/nginx.conf

server {
    listen 10.7.1.13:443 ssl;
    server_name web.tp7.secu;

    ssl_certificate /etc/pki/tls/certs/web.tp7.secu.crt;
    ssl_certificate_key /etc/pki/tls/private/web.tp7.secu.key;
    
    location / {
        root /var/www/site_nul;
        index index.html;
    }
}

[dora@web ~]$ sudo systemctl restart nginx
```

🌞 **Prouvez avec un `curl` que vous accédez au site web**

- depuis votre PC
- avec un `curl -k` car il ne reconnaît pas le certificat là

```
C:\Users\ZerBr>curl -k https://10.7.1.13
```

🌞 **Ajouter le certificat de la CA dans votre navigateur**

- vous pourrez ensuite visitez `https://web.tp7.b2` sans alerte de sécurité, et avec un cadenas vert
- il faut aussi ajouter l'IP de la machine à votre fichier `hosts` pour qu'elle corresponde au nom `web.tp7.b2`
